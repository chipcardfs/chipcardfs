#!/usr/bin/make -f 
# (c) 2011 Christoph Grenz <christophg+gitorious@grenz-bonn.de>
# Licensed under GPLv3

SUBSYSTEMS = python-chipcard
PYTHON_CONFIG = /usr/bin/python-config
PREFIX=/usr
PYLIBDIR := $(shell $(PYTHON_CONFIG) --prefix)/lib/$(shell $(PYTHON_CONFIG) --libs | sed 's/.*-l\(python[0-9.]*\).*/\1/')

all: $(SUBSYSTEMS)

$(SUBSYSTEMS):
	cd $@ && $(MAKE)

%-clean:
	cd $(patsubst %-clean,%,$@) && $(MAKE) clean
clean: $(patsubst %,%-clean,$(SUBSYSTEMS))

%-install:
	cd $(patsubst %-install,%,$@) && $(MAKE) install
install: $(patsubst %,%-install,$(SUBSYSTEMS))
	install -m 755 chipcardfs.py $(PYLIBDIR)/
	install -m 755 chipcardfs $(PREFIX)/bin/
	python -m compileall $(PYLIBDIR)/chipcardfs.py
	python -OO -m compileall $(PYLIBDIR)/chipcardfs.py

%-uninstall:
	cd $(patsubst %-uninstall,%,$@) && $(MAKE) uninstall
uninstall: $(patsubst %,%-uninstall,$(SUBSYSTEMS))
	rm -f $(PYLIBDIR)/chipcardfs.pyo
	rm -f $(PYLIBDIR)/chipcardfs.pyc
	rm -f $(PYLIBDIR)/chipcardfs.py
	rm -f $(PREFIX)/bin/chipcardfs

.PHONY: all $(SUBSYSTEMS)
