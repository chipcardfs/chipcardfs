#!/usr/bin/env pyrexc
# (c) 2011 Christoph Grenz <christophg+gitorious@grenz-bonn.de>
# Licensed under GPLv3

import math
import errno as _errno

cimport gwen_types as gwen
from gwen_types cimport GWEN_TYPE_UINT32, GWEN_STRINGLIST, GWEN_BUFFER, GWEN_STRINGLISTENTRY

cdef extern from "Python.h":
	ctypedef int Py_ssize_t
	object PyString_FromStringAndSize(char *s, Py_ssize_t len)

cdef extern from "chipcard/chipcard.h":

	cdef enum error_codes:
		LC_ERROR_NONE,
		LC_ERROR_GENERIC,
		LC_ERROR_INVALID,
		LC_ERROR_CARD_REMOVED,
		LC_ERROR_CARD_NOT_OWNED,
		LC_ERROR_NOT_SUPPORTED,
		LC_ERROR_SETUP,
		LC_ERROR_NO_DATA,
		LC_ERROR_LOCKED_BY_OTHER,
		LC_ERROR_NOT_LOCKED,
		LC_ERROR_BAD_RESPONSE,
		LC_ERROR_NO_SLOTS_CONNECTED,
		LC_ERROR_NO_SLOTS_DISCONNECTED,
		LC_ERROR_NO_SLOTS_AVAILABLE,
		LC_ERROR_BAD_PIN,
		LC_ERROR_USER_ABORTED,
		LC_ERROR_CARD_DESTROYED

	cdef enum reader_flags:
		LC_READER_FLAGS_KEYPAD,
		LC_READER_FLAGS_DISPLAY,
		LC_READER_FLAGS_NOINFO,
		LC_READER_FLAGS_REMOTE,
		LC_READER_FLAGS_AUTO,
		LC_READER_FLAGS_SUSPENDED_CHECKS,
		LC_READER_FLAGS_DRIVER_HAS_VERIFY

	char* LC_Error_toString(gwen.GWEN_TYPE_UINT32 err)

	cdef enum LC_DRIVER_STATUS:
		LC_DriverStatusDown,
		LC_DriverStatusWaitForStart,
		LC_DriverStatusStarted,
		LC_DriverStatusUp,
		LC_DriverStatusStopping,
		LC_DriverStatusAborted,
		LC_DriverStatusDisabled,
		LC_DriverStatusUnknown

	cdef enum LC_READER_STATUS:
		LC_ReaderStatusDown,
		LC_ReaderStatusWaitForStart,
		LC_ReaderStatusWaitForDriver,
		LC_ReaderStatusWaitForReaderUp,
		LC_ReaderStatusWaitForReaderDown,
		LC_ReaderStatusUp,
		LC_ReaderStatusAborted,
		LC_ReaderStatusDisabled,
		LC_ReaderStatusUnknown

	LC_READER_STATUS LC_ReaderStatus_fromString(char* s)
	char *LC_ReaderStatus_toString(LC_READER_STATUS rst)

	cdef enum LC_SERVICE_STATUS:
		LC_ServiceStatusDown,
		LC_ServiceStatusWaitForStart,
		LC_ServiceStatusStarted,
		LC_ServiceStatusUp,
		LC_ServiceStatusSilentRunning,
		LC_ServiceStatusStopping,
		LC_ServiceStatusAborted,
		LC_ServiceStatusDisabled,
		LC_ServiceStatusUnknown

	LC_SERVICE_STATUS LC_ServiceStatus_fromString(char *s)
	char* LC_ServiceStatus_toString(LC_SERVICE_STATUS st)

	cdef enum LC_CARD_STATUS:
		LC_CardStatusInserted,
		LC_CardStatusRemoved,
		LC_CardStatusOrphaned,
		LC_CardStatusUnknown

	cdef enum LC_CARD_TYPE:
		LC_CardTypeUnknown,
		LC_CardTypeProcessor,
		LC_CardTypeMemory

from client_iface cimport LC_CLIENT, LC_CLIENT_RESULT, LC_Client_ResultOk, LC_CLIENT_CMDTARGET, LC_Client_CmdTargetCard
from card_iface cimport LC_CARD

cdef extern from *:
	ctypedef void* LC_CARD_LIST2

cdef extern from "chipcard/card.h":

	LC_CLIENT_RESULT LC_Card_Open(LC_CARD* card)
	LC_CLIENT_RESULT LC_Card_Close(LC_CARD* card)
	void LC_Card_free(LC_CARD* cd)
	void LC_Card_List2_freeAll(LC_CARD_LIST2* l)
	LC_CLIENT_RESULT LC_Card_Check(LC_CARD* card)
	LC_CLIENT_RESULT LC_Card_Reset(LC_CARD* card)

	#GWEN_TYPE_UINT32 LC_Card_GetCardId(LC_CARD *cd)
	GWEN_TYPE_UINT32 LC_Card_GetReaderFlags(LC_CARD *cd)
	char* LC_Card_GetCardType(LC_CARD* cd)
	GWEN_STRINGLIST* LC_Card_GetCardTypes(LC_CARD* cd)
	unsigned int LC_Card_GetAtr(LC_CARD *cd, unsigned char **pbuf)
	LC_CLIENT* LC_Card_GetClient(LC_CARD* cd)

	LC_CLIENT_RESULT LC_Card_ExecApdu(LC_CARD *card,
                                  char *apdu,
                                  unsigned int len,
                                  GWEN_BUFFER *rbuf,
                                  int t)

cdef extern from "chipcard/client.h":


	LC_CLIENT* LC_Client_new(char *programName, char* programVersion)
	void LC_Client_free(LC_CLIENT* cl)

	int LC_Client_Init(LC_CLIENT *cl)

	int LC_Client_GetShortTimeout(LC_CLIENT *cl)
	int LC_Client_GetLongTimeout(LC_CLIENT *cl)

	LC_CLIENT_RESULT LC_Client_Start(LC_CLIENT *cl)
	LC_CLIENT_RESULT LC_Client_Stop(LC_CLIENT *cl)
	LC_CLIENT_RESULT LC_Client_Fini(LC_CLIENT *cl)
	LC_CLIENT_RESULT LC_Client_GetNextCard(LC_CLIENT *cl, LC_CARD **pCard, int timeout)
	LC_CLIENT_RESULT LC_Client_ReleaseCard(LC_CLIENT *cl, LC_CARD *card)


cdef object interface_dict
interface_dict = {}


def _statuscode2errno(code, instruction=None):
	sw1 = code[0]
	sw2 = 0
	if len(code) > 1:
		sw2 = code[1]
	if sw1 == '\x90':
		return 0
	if sw1 == '\x62':
		if sw2 == '\x81':
			return _errno.EIO
		if sw2 == '\x82':
			return _errno.ERANGE
		return 0 # Only warnings
	if sw1 == '\x63':
		if sw2 == '\x81':
			return _errno.ENOSPC
		if instruction == '\x20' or instruction == '\xB2':
			# VERIFY or EXT. AUTHENTICATE
			return _errno.EKEYREJECTED
		if ord(sw2) & 0xC0 == 0xC0:
			# Counter is only a warning
			return 0
		return _errno.EIO
	if sw1 == '\x64':
		return _errno.EIO
	if sw1 == '\x65':
		return _errno.EIO
	if sw1 == '\x66':
		# Security errors
		return _errno.ENOKEY
	if sw1 == '\x67':
		# Wrong length
		return _errno.ESPIPE
	if sw1 == '\x68':
		# Class function not supported
		return _errno.EPROTO
	if sw1 == '\x69':
		# Command not allowed
		if sw2 == '\x81':
			return _errno.EPROTOTYPE
		if sw2 == '\x82':
			return _errno.EACCES
		if sw2 == '\x83':
			return _errno.EKEYEXPIRED
		return _errno.EPERM
	if sw1 == '\x6A':
		# Wrong P1-P2
		if sw2 == '\x81':
			return _errno.EPROTOTYPE
		if sw2 == '\x82':
			return _errno.ENOENT
		if sw2 == '\x83':
			return _errno.ENOMSG
		if sw2 == '\x84':
			return _errno.EOVERFLOW
		if sw2 == '\x88':
			return _errno.ENOLINK
		return _errno.EINVAL
	if sw1 == '\x6B':
		# Wrong P1-P2
		return _errno.EINVAL
	if sw1 == '\x6C':
		# Wrong length
		return _errno.ESPIPE
	if sw1 == '\x6D':
		# Instruction not supported
		return _errno.ENOTSUP
	if sw1 == '\x6E':
		# Class not supported
		return _errno.EPROTONOSUPPORT
	if sw1 == '\x6F':
		# Other
		return _errno.EREMOTEIO


class ChipCardClientError(Exception):
	pass

class CardException(IOError):

	def __init__(self, status, instruction=None, errno=None, data=None):
		if errno is None:
			errno = _statuscode2errno(status, instruction)
		super(CardException, self).__init__(errno, status)
		self.instruction = instruction
		self.data = data

	def status(self):
		return self[1]
	status = property(status)

	def __str__(self):
		r = 'error code %02X%02X' % (ord(self[1][0]), ord(self[1][1]))
		if self.instruction:
			r += ' on %02X' % self.instruction
		if self.errno > 0:
			r += ' (%s)' % _errno.errorcode[self.errno]
		return r

	def __repr__(self):
		r = '<CardException'
		if self.instruction:
			r += ' on %02X' % self.instruction
		r += ': %02X%02X %r>' % (ord(self.status[0]), ord(self.status[1]), self.data)
		return r

class CardResponse(object):

	def __init__(self, data, instruction=None):
		self.instruction = instruction
		self.data = data[:-2]
		self.status = s = data[-2:]

	def __getitem__(self, i):
		return self.data[i]

	def has_warning(self):
		return self.status[0] in ('\x62', '\x63')
	has_warning = property(has_warning)

	def corrupted(self):
		return self.status == '\x62\x81'
	corrupted = property(corrupted)

	def invalidated(self):
		return self.status == '\x62\x83'
	invalidated = property(invalidated)

	def nonstandard_fci(self):
		return self.status == '\x62\x84'
	nonstandard_fci = property(nonstandard_fci)

	def eof_reached(self):
		return self.status == '\x62\x82' or self.status == '\x63\x81'
	eof_reached = property(eof_reached)

	def counter(self):
		if self.status[0] in ('\x62', '\x63'):
			if ord(self.status[1]) & 0xC0 == 0xC0:
				return ord(self.status[1]) & 0xF
		return False
	counter = property(counter)

	def __len__(self):
		return len(self.data)

	def __iter__(self):
		return iter(self.data)

	def __str__(self):
		return self.data

	def __unicode__(self):
		return self.data

	def __repr__(self):
		r = '<CardResponse'
		if self.instruction:
			r += ' to %02X' % self.instruction
		r += ': %02X%02X %r>' % (ord(self.status[0]), ord(self.status[1]), self.data)
		return r


class UnsupportedInterface(Exception):

	def __init__(self, x):
		Exception.__init__(self, 'Card type "%s" is unsupported by this python wrapper' % x)

cdef object GwenStringListToList(gwen.GWEN_STRINGLIST* l):
	cdef gwen.GWEN_STRINGLISTENTRY* x
	r = []
	x = gwen.GWEN_StringList_FirstEntry(l)
	while x is not NULL:
		r.append(gwen.GWEN_StringListEntry_Data(x))
		x = gwen.GWEN_StringListEntry_Next(x)
	return r

cdef class Card:
	cdef LC_CARD* cobj
	cdef object __client
	cdef LC_CLIENT* clientobj
	cdef object __types
	cdef object __type
	cdef object __currentname
	cdef object __currentobj
	cdef object delegated
	cdef object freed

	cdef void set_cobj(Card self, LC_CARD* new):
		self.cobj = new

	def __init__(self):
		cdef gwen.GWEN_STRINGLIST* l
		if self.cobj is NULL:
			raise TypeError, "cannot create 'Card' instances; use 'Client' instead"
		l = LC_Card_GetCardTypes(self.cobj)
		self.delegated = False
		self.freed = False

		types = GwenStringListToList(l)
		gwen.GWEN_StringList_free(l)
		if 'MemoryCard' in types:
			types.append('raw_memory')
		self.__types = tuple(types)

		self.__type = LC_Card_GetCardType(self.cobj)
		self.__currentname = None
		self.__currentobj = None

	def __del__(self):
		if self.cobj is not NULL:
			self.free()

	def __repr__(self):
		if self.freed:
			return '<freed chipcard object>'
		return '<chipcard #%i of type "%s">' % (self.id, self.__type)

	property id:
		def __get__(self):
			if self.freed:
				raise RuntimeError, 'Card object already freed'
			return -1
			#return LC_Card_GetCardId(self.cobj)

	property atr:
		def __get__(self):
			if self.freed:
				raise RuntimeError, 'Card object already freed'
			cdef unsigned char* ptr
			cdef int r
			r = LC_Card_GetAtr(self.cobj, &ptr)
			if r >= 0:
				return PyString_FromStringAndSize(<char*>ptr, r)
			else:
				raise RuntimeError, 'Error while reading ATR: %i' % r


	property type:
		def __get__(self):
			if self.freed:
				raise RuntimeError, 'Card object already freed'
			return self.__type

	property interfaces:
		def __get__(self):
			if self.freed:
				raise RuntimeError, 'Card object already freed'
			return self.__types

	def execute(self, instruction, p1, p2, data, Le, channel=0):
		if not 0 <= channel < 4:
			raise ValueError, 'invalid channel'
		if not 0 <= instruction <= 0xFF:
			raise ValueError, 'instruction code too big'
		if not 0 <= p1 <= 0xFF:
			raise ValueError, 'p1 too big'
		if not 0 <= p2 <= 0xFF:
			raise ValueError, 'p2 too big'
		if not 0 <= Le <= 0xFF:
			raise ValueError, 'Le too big'
		if data is not None and not 0 <= len(data) <= 0xFF:
			raise ValueError, 'Data too big'
		CLS = chr(channel)
		INS = chr(instruction)
		P1 = chr(p1)
		P2 = chr(p2)
		LC = ''
		DATA = ''
		if data is not None:
			LC = chr(len(data))
			DATA = data
		LE = ''
		if Le is not None:
			LE = chr(Le)

		result = self.execRawApdu(CLS+INS+P1+P2+LC+DATA+LE)

		code = result[-2:]
		if code[0] == '\x61':
			# get rest of data
			result2 = self.execute(0xC0, 0, 0, None, 0xFF, channel=channel)
			result.data += result2.data
			result.status = result2.status
		if ord(code[0]) & 0x90 == 0x90 or code[0] in '\x62\x63':
			return CardResponse(result, instruction)
		else:
			raise CardException(code, instruction, None, result[:-2])

	def execRawApdu(self, apdu):
		cdef GWEN_BUFFER* buf
		cdef int r

		if self.freed:
			raise RuntimeError, 'Card object already freed'

		buf = gwen.GWEN_Buffer_new(NULL, 65538, 0, 0)

		try:
			r = LC_Card_ExecApdu(self.cobj, apdu, len(apdu), buf, LC_Client_CmdTargetCard)
			if r != LC_Client_ResultOk:
				raise ChipCardClientError, r
			return PyString_FromStringAndSize(gwen.GWEN_Buffer_GetStart(buf), gwen.GWEN_Buffer_GetUsedBytes(buf) )
		finally:
			gwen.GWEN_Buffer_free(buf)

	def getInterface(self, interface):
		cdef LC_CARD* tmp
		if self.freed:
			raise RuntimeError, 'Card object already freed'

		if interface not in self.__types:
			raise ValueError, 'item not in list'
		if interface not in interface_dict:
			raise UnsupportedInterface, interface
		if self.__currentname is None:
			i = createInterface(interface_dict[interface], self.cobj)
			self.__currentobj = i
			self.__currentname = interface
			self.delegated = True
			return i
		elif self.__currentname == interface:
			return self.__currentobj
		else:
			raise Exception, 'Another interface is already open'

	def free(self):
		if self.delegated:
			return False
		if self.freed:
			raise RuntimeError, 'Card object already freed'
		LC_Client_ReleaseCard(self.clientobj, self.cobj)
		#LC_Card_free(self.cobj)
		self.cobj = NULL
		self.freed = True

cdef class AbstractCardInterface:
	cdef LC_CARD* cobj

cdef AbstractCardInterface createInterface(object T, LC_CARD* c):
	cdef AbstractCardInterface interface
	interface = T.__new__(T)
	interface.cobj = c
	T.__init__(interface)
	return interface

cimport memorycard
cdef class MemoryCardInterface(AbstractCardInterface):
	cdef object freed

	def __init__(self):
		memorycard.LC_MemoryCard_ExtendCard(self.cobj)
		LC_Card_Open(self.cobj)
		self.freed = False

	def __del__(self):
		if not self.freed:
			self.free()

	def free(self):
		if self.freed:
			raise RuntimeError, 'Card interface object already freed!'
		LC_Card_Close(self.cobj)
		memorycard.LC_MemoryCard_UnextendCard(self.cobj)
		LC_Card_free(self.cobj)
		self.cobj = NULL
		self.freed = True

	cdef object read_data(MemoryCardInterface self, int offset, int size):
		cdef GWEN_BUFFER* buf
		cdef int r
		buf = gwen.GWEN_Buffer_new(NULL, size+2, 0, 0)
		try:
			r = memorycard.LC_MemoryCard_ReadBinary(self.cobj, offset, size, buf)
			if r != LC_Client_ResultOk:
				raise ChipCardClientError, r
			return PyString_FromStringAndSize(gwen.GWEN_Buffer_GetStart(buf), min(size, gwen.GWEN_Buffer_GetUsedBytes(buf)))
		finally:
			gwen.GWEN_Buffer_free(buf)

	def readBinary(self, offset=0, size=-1):
		if self.freed:
			raise RuntimeError, 'Card interface object already freed!'
		if size == -1:
			size = self.capacity - offset
			if size <= 0:
				raise Exception, 'Amount of data to read could not be determined!'
		return self.read_data(offset, size)


	def writeBinary(self, data, offset=0):
		if self.freed:
			raise RuntimeError, 'Card interface object already freed!'
		r = memorycard.LC_MemoryCard_WriteBinary(self.cobj, offset, data, len(data))
		print r

	property capacity:
		def __get__(self):
			if self.freed:
				raise RuntimeError, 'Card interface object already freed!'
			return memorycard.LC_MemoryCard_GetCapacity(self.cobj)

interface_dict['MemoryCard'] = MemoryCardInterface

cdef class RawMemCardInterface(AbstractCardInterface):
	cdef object freed

	def __init__(self):
		memorycard.LC_MemoryCard_ExtendCard(self.cobj)
		LC_Card_Open(self.cobj)
		self.freed = False

	def __del__(self):
		if not self.freed:
			self.free()

	def free(self):
		if self.freed:
			raise RuntimeError, 'Card interface object already freed!'
		LC_Card_Close(self.cobj)
		memorycard.LC_MemoryCard_UnextendCard(self.cobj)
		LC_Card_free(self.cobj)
		self.cobj = NULL
		self.freed = True

	cdef object read_data(RawMemCardInterface self, int offset, int size):
		cdef GWEN_BUFFER* buf
		cdef int r
		buf = gwen.GWEN_Buffer_new(NULL, size+2, 0, 0)
		try:
			r = memorycard.LC_MemoryCard_ReadBinary(self.cobj, offset, size, buf)
			if r != LC_Client_ResultOk:
				raise ChipCardClientError, r
			return PyString_FromStringAndSize(gwen.GWEN_Buffer_GetStart(buf), min(size, gwen.GWEN_Buffer_GetUsedBytes(buf)))
		finally:
			gwen.GWEN_Buffer_free(buf)

	def readBinary(self, offset=0, size=-1):
		if self.freed:
			raise RuntimeError, 'Card interface object already freed!'
		if size == -1:
			size = self.capacity - offset
			if size <= 0:
				raise Exception, 'Amount of data to read could not be determined!'
		return self.read_data(offset, size)

	cdef object write_data(RawMemCardInterface self, int offset, object data):
		cdef GWEN_BUFFER* buf
		cdef int r, target
		target = LC_Client_CmdTargetCard
		buf = gwen.GWEN_Buffer_new(NULL, 2, 0, 0)
		try:
			flag1 = chr(0)
			flag2 = chr(offset)
			length = chr(len(data))
			apdu = '\x00\xD6%s%s%s%s' % (flag1, flag2, length, data)
			r = LC_Card_ExecApdu(self.cobj, apdu, len(apdu), buf, target)
			if r != LC_Client_ResultOk:
				raise ChipCardClientError, r
		finally:
			gwen.GWEN_Buffer_free(buf)

	def writeBinary(self, data, offset=0, bufsize=9):
		if self.freed:
			raise RuntimeError, 'Card interface object already freed!'
		parts = []
		for x in xrange(int(math.ceil(len(data)/float(int(bufsize))))):
			parts.append(data[(x*bufsize):(x*bufsize+bufsize)])
		x = 0
		for part in parts:
			self.write_data(offset+(x*bufsize), part)
			x = x + 1
		return

	property capacity:
		def __get__(self):
			if self.freed:
				raise RuntimeError, 'Card interface object already freed!'
			return memorycard.LC_MemoryCard_GetCapacity(self.cobj)

interface_dict['raw_memory'] = RawMemCardInterface

cdef Card createCard(LC_CARD* c, LC_CLIENT* clobj, object cl):
	cdef Card cd
	cd = Card.__new__(Card)
	cd.cobj = c
	cd.clientobj = clobj
	cd.__client = cl
	cd.__init__()
	return cd

cdef class Client:
	cdef LC_CLIENT* cobj

	def __init__(self, programName='Python ChipCard2 interface', programVersion='0.1'):
		self.cobj = LC_Client_new(programName, programVersion)
		LC_Client_Init(self.cobj)

	def startWait(self):
		if self.cobj is NULL:
			raise RuntimeError, 'Client object already freed!'
		LC_Client_Start(self.cobj)

	def getNextCard(self, timeout=30):
		if self.cobj is NULL:
			raise RuntimeError, 'Client object already freed!'
		cdef LC_CARD* cd
		cd = NULL
		LC_Client_GetNextCard(self.cobj, &cd, timeout)
		if cd is NULL:
			return None
		return createCard(cd, self.cobj, self)

	def stopWait(self):
		if self.cobj is NULL:
			raise RuntimeError, 'Client object already freed!'
		LC_Client_Stop(self.cobj);

	def free(self):
		if self.cobj is NULL:
			raise RuntimeError, 'Client object already freed!'
		LC_Client_free(self.cobj)
		self.cobj = NULL

	def __del__(self):
		if self.cobj is not NULL:
			self.free()


