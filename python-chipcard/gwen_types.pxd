# (c) 2011 Christoph Grenz <christophg+gitorious@grenz-bonn.de>
# Licensed under GPLv3

cdef extern from *:
	ctypedef int GWEN_TYPE_UINT32
	ctypedef void* GWEN_BUFFER
	ctypedef void* GWEN_XMLNODE
	ctypedef void* GWEN_DB_NODE
	ctypedef void* GWEN_STRINGLIST
	ctypedef void* GWEN_STRINGLISTENTRY
	
	GWEN_STRINGLIST *GWEN_StringList_new()
	void GWEN_StringList_free(GWEN_STRINGLIST *sl)
	GWEN_STRINGLIST *GWEN_StringList_dup(GWEN_STRINGLIST *sl)
	void GWEN_StringList_Clear(GWEN_STRINGLIST *sl)
	unsigned int GWEN_StringList_Count(GWEN_STRINGLIST *sl)
	GWEN_STRINGLISTENTRY *GWEN_StringListEntry_new(char *s, int take)
	void GWEN_StringListEntry_ReplaceString(GWEN_STRINGLISTENTRY *e, char *s, int take)
	void GWEN_StringListEntry_free(GWEN_STRINGLISTENTRY *sl)
	void GWEN_StringList_AppendEntry(GWEN_STRINGLIST *sl, GWEN_STRINGLISTENTRY *se)
	void GWEN_StringList_RemoveEntry(GWEN_STRINGLIST *sl, GWEN_STRINGLISTENTRY *se)
	GWEN_STRINGLISTENTRY *GWEN_StringList_FirstEntry(GWEN_STRINGLIST *sl)
	GWEN_STRINGLISTENTRY *GWEN_StringListEntry_Next(GWEN_STRINGLISTENTRY *se)
	char *GWEN_StringListEntry_Data(GWEN_STRINGLISTENTRY *se)
	void GWEN_StringListEntry_SetData(GWEN_STRINGLISTENTRY *se, char *s)
	void GWEN_StringList_SetSenseCase(GWEN_STRINGLIST *sl, int i)
	void GWEN_StringList_SetIgnoreRefCount(GWEN_STRINGLIST *sl, int i)
	int GWEN_StringList_AppendString(GWEN_STRINGLIST *sl, char *s, int take, int checkDouble)
	int GWEN_StringList_InsertString(GWEN_STRINGLIST *sl, char *s, int take, int checkDouble)
	int GWEN_StringList_RemoveString(GWEN_STRINGLIST *sl, char *s)
	int GWEN_StringList_HasString(GWEN_STRINGLIST *sl, char *s)
	char *GWEN_StringList_FirstString(GWEN_STRINGLIST *l)
	char *GWEN_StringList_StringAt(GWEN_STRINGLIST *l, int idx)

	GWEN_BUFFER* GWEN_Buffer_new(char *buffer,
                             GWEN_TYPE_UINT32 size,
                             GWEN_TYPE_UINT32 used,
                             int take_ownership)
	void GWEN_Buffer_free(GWEN_BUFFER *bf)
	char *GWEN_Buffer_GetStart(GWEN_BUFFER *bf)
	GWEN_TYPE_UINT32 GWEN_Buffer_GetSize(GWEN_BUFFER *bf)
	GWEN_TYPE_UINT32 GWEN_Buffer_GetUsedBytes(GWEN_BUFFER *bf)