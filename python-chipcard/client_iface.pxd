# (c) 2011 Christoph Grenz <christophg+gitorious@grenz-bonn.de>
# Licensed under GPLv3


cdef extern from "chipcard/client.h":
	struct LC_CLIENT:
		pass
	cdef enum LC_CLIENT_RESULT:
		LC_Client_ResultOk,
		LC_Client_ResultWait,
		LC_Client_ResultIpcError,
		LC_Client_ResultCmdError,
		LC_Client_ResultDataError,
		LC_Client_ResultAborted,
		LC_Client_ResultInvalid,
		LC_Client_ResultInternal,
		LC_Client_ResultGeneric,
		LC_Client_ResultNoData,
		LC_Client_ResultCardRemoved,
		LC_Client_ResultNotSupported

	cdef enum LC_CLIENT_CMDTARGET:
		LC_Client_CmdTargetCard,
		LC_Client_CmdTargetReader
