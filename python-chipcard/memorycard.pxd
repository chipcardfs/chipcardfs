# (c) 2011 Christoph Grenz <christophg+gitorious@grenz-bonn.de>
# Licensed under GPLv3

cimport gwen_types as gwen
from client_iface cimport LC_CLIENT_RESULT
from card_iface cimport LC_CARD

cdef extern from "chipcard/cards/memorycard.h":
	int LC_MemoryCard_ExtendCard(LC_CARD *card)
	int LC_MemoryCard_UnextendCard(LC_CARD *card)
	LC_CLIENT_RESULT LC_MemoryCard_ReadBinary(LC_CARD *card, int offset, int size, gwen.GWEN_BUFFER *buf)
	LC_CLIENT_RESULT LC_MemoryCard_WriteBinary(LC_CARD *card, int offset, char *ptr, unsigned int size)
	unsigned int LC_MemoryCard_GetCapacity(LC_CARD *card)
