#!/usr/bin/python
# -*- coding: utf-8 -*-
# (c) 2011 Christoph Grenz <christophg+gitorious@grenz-bonn.de>
# Licensed under GPLv3

import chipcard
import fuse
from fuse import Fuse
from threading import RLock
from functools import wraps

from time import time

from stat import S_IFDIR, S_IFREG, S_IFLNK    # for file properties
import os      # for filesystem modes (O_RDONLY, etc)
import errno   # for error number codes (ENOENT, etc)
			# - note: these must be returned as negatives
import sys, collections


fuse.fuse_python_api = (0,2)

__metaclass__ = type

def dirFromList(list):
	"""
	Return a properly formatted list of items suitable to a directory listing.
	[['a', 'b', 'c']] => [[('a', 0), ('b', 0), ('c', 0)]]
	"""
	return [[(x, 0) for x in list]]

def split_path(path):
	"""
	Return the slash-separated parts of a given path as a list
	"""
	if path == '':
		return ['.']
	if path == '.':
		return ['.']
	elif path == '/':
		return []
	else:
		head, tail = os.path.split(path)
		return split_path(head)+[tail]

def hexcode(s):
	return ''.join('%02X' % ord(c) for c in s)

_Stat = collections.namedtuple('Stat', ['st_ino', 'st_dev', 'st_blksize', 'st_mode', 'st_nlink', 'st_uid', 'st_gid', 'st_rdev', 'st_size', 'st_blocks', 'st_atime', 'st_mtime', 'st_ctime'])

def parseDIR(data):
	while True:
		cur = {}
		if len(data) == 0:
			return
		typ = ord(data[0])
		if typ == 0x61:
			size = ord(data[1])
			content = data[2:2+size]
			yield parseDIRentry(content)
			data = data[2+size:]
		else:
			# format not supported, abort
			return

def parseDIRentry(data):
	cur = { 'AID':None, 'label':None, 'ref':[], 'url':[], 'ddata':[], 'dtemplate':[], 'cmdapdu':[], 'sub':[] }
	while True:
		if len(data) < 2:
			break
		typ = ord(data[0])
		if typ == 0x4F:
			size = ord(data[1])
			content = data[2:2+size]
			cur['AID'] = content
			data = data[2+size:]
		elif typ == 0x50:
			size = ord(data[1])
			content = data[2:2+size]
			cur['label'] = content
			data = data[2+size:]
		elif typ == 0x51:
			size = ord(data[1])
			content = data[2:2+size]
			cur['ref'].append(content)
			data = data[2+size:]
		elif typ == 0x52:
			size = ord(data[1])
			content = data[2:2+size]
			cur['cmdapdu'].append(content)
			data = data[2+size:]
		elif typ == 0x53:
			size = ord(data[1])
			content = data[2:2+size]
			cur['ddata'].append(content)
			data = data[2+size:]
		elif typ == 0x73:
			size = ord(data[1])
			content = data[2:2+size]
			cur['dtemplate'].append(content)
			data = data[2+size:]
		elif typ == 0x5F:
			size = ord(data[2])
			content = data[3:3+size]
			cur['url'].append(content)
			data = data[3+size:]
		elif typ == 0x61:
			size = ord(data[1])
			content = data[2:2+size]
			cur['sub'].append(content)
			data = data[2+size:]
		else:
			size = ord(data[1])
			content = data[2:2+size]
			cur['%02X' % typ] = content
	return cur

def parseFileInfo(data):
	if data[0] == '\x6F' and ord(data[1]) == len(data)-2:
		data = data[2:]
	if data[0] == '\x62' and ord(data[1]) == len(data)-2:
		data = data[2:]
	
	result = { 'datasize': None, 'size':None, 'shareable': None, 'category':None, 'structure':None, 'tlv': False, 'maxrecordsize': None, 'recordcount': None, 'writebehaviour': None, 'unitsize': None, 'longprivatetags': None, 'tlvefs': None, 'id':[], 'name':[], 'proprietary':[], 'security-proprietary':[], 'fci-extension': None, 'short-ef': None, 'lifecycle': None, 'active': None, 'security-expanded': None, 'security-short': None, 'security-template-ef': [], 'channel-security': None, 'proprietary-tlv': None }
	while True:
		if len(data) < 2:
			break
		typ = ord(data[0])
		if typ == 0x80:
			size = ord(data[1])
			content = data[2:2+size]
			result['datasize'] = content
			data = data[2+size:]
		elif typ == 0x81:
			size = ord(data[1])
			content = data[2:2+size]
			result['size'] = content
			data = data[2+size:]
		elif typ == 0x82:
			size = ord(data[1])
			content = data[2:2+size]
			fdb = ord(content[0])
			result['shareable'] = bool(fdb & 64)
			if fdb & 0x38 == 0x38:
				result['category'] = 'DF'
			elif fdb & 0x38 == 0:
				result['category'] = 'EF-working'
			elif fdb & 0x38 == 8:
				result['category'] = 'EF-internal'
			else:
				result['category'] = 'EF-other'
			
			if fdb & 0x38 == 0x38 and fdb & 0x07 == 1:
				result['structure'] = 'BER-TLV'
				result['tlv'] = True
			elif fdb & 0x38 == 0x38 and fdb & 0x07 == 2:
				result['structure'] = 'SIMPLE-TLV'
				result['tlv'] = True
			elif fdb & 0x07 == 0:
				result['structure'] = None
			elif fdb & 0x07 == 1:
				result['structure'] = 'transparent'
			elif fdb & 0x07 == 2:
				result['structure'] = 'linear-fixed'
			elif fdb & 0x07 == 3:
				result['structure'] = 'linear-fixed'
				result['tlv'] = True
			elif fdb & 0x07 == 4:
				result['structure'] = 'linear-variable'
			elif fdb & 0x07 == 5:
				result['structure'] = 'linear-variable'
				result['tlv'] = True
			elif fdb & 0x07 == 6:
				result['structure'] = 'cyclic-fixed'
			elif fdb & 0x07 == 7:
				result['structure'] = 'cyclic-fixed'
				result['tlv'] = True
			
			if len(content) >= 2:
				dcb = ord(content[1])
				result['tlvefs'] = bool(dcb & 128)
				result['longprivatetags'] = bool(dcb & 16)
				
				if dcb & 0x60 == 0:
					result['writebehaviour'] = 'one-time write'
				elif dcb & 0x60 == 32:
					result['writebehaviour'] = 'proprietary'
				elif dcb & 0x60 == 64:
					result['writebehaviour'] = 'write-or'
				elif dcb & 0x60 == 0x60:
					result['writebehaviour'] = 'write-and'
				
				unitsize = 2**(dcb & 0x0F) / 2.0
			
			if len(content) == 3:
				result['maxrecordsize'] = ord(content[2])
			
			if len(content) >= 4:
				result['maxrecordsize'] = ord(content[2])*256+ord(content[3])
			
			if len(content) == 5:
				result['recordcount'] = ord(content[4])
			
			if len(content) == 6:
				result['recordcount'] = ord(content[4])*256+ord(content[5])
			data = data[2+size:]
		
		elif typ == 0x83:
			size = ord(data[1])
			content = data[2:2+size]
			result['id'].append(content)
			data = data[2+size:]
		elif typ == 0x84:
			size = ord(data[1])
			content = data[2:2+size]
			result['name'].append(content)
			data = data[2+size:]
		elif typ == 0x85:
			size = ord(data[1])
			content = data[2:2+size]
			result['proprietary'].append(content)
			data = data[2+size:]
		elif typ == 0x86:
			size = ord(data[1])
			content = data[2:2+size]
			result['security-proprietary'].append(content)
			data = data[2+size:]
		elif typ == 0x87:
			size = ord(data[1])
			content = data[2:2+size]
			result['fci-extension'] = content
			data = data[2+size:]
		elif typ == 0x88:
			size = ord(data[1])
			content = data[2:2+size]
			result['short-ef'] = content
			data = data[2+size:]
		elif typ == 0x8A:
			size = ord(data[1])
			content = data[2:2+size]
			lcsb = ord(content[0])
			if lcsb == 0:
				result['lifecycle'] = None
			elif lcsb == 1:
				result['lifecycle'] = 'creation'
			elif lcsb == 2:
				result['lifecycle'] = 'initialisation'
			elif lcsb < 8:
				result['lifecycle'] = 'operational'
				result['active'] = True if lcsb & 1 else False
			elif lcsb < 16:
				result['lifecycle'] = 'termination'
			else:
				result['lifecycle'] = 'other'
			data = data[2+size:]
		elif typ == 0x8B:
			size = ord(data[1])
			content = data[2:2+size]
			result['security-expanded'] = content
			data = data[2+size:]
		elif typ == 0x8C:
			size = ord(data[1])
			content = data[2:2+size]
			result['security-short'] = content
			data = data[2+size:]
		elif typ == 0x8D:
			size = ord(data[1])
			content = data[2:2+size]
			result['security-template-ef'].append(content)
			data = data[2+size:]
		elif typ == 0x8E:
			size = ord(data[1])
			content = data[2:2+size]
			csb = ord(content[0])
			chsec = set()
			if (csb & 1):
				chsec.add('not-shareable')
			if (csb & 2):
				chsec.add('secured')
			if (csb & 4):
				chsec.add('user-authenticated')
			result['channel-security'] = frozenset(chsec)
			data = data[2+size:]
		elif typ == 0xA5:
			size = ord(data[1])
			content = data[2:2+size]
			result['proprietary-tlv'] = content
			data = data[2+size:]
		else:
			size = ord(data[1])
			data = data[2+size:]
	return result

def hexstr(s):
	return ' '.join('%02x' % ord(c) for c in s)


mutex = RLock()
def serialized(lck):
	
	@wraps
	def _serialized_wrapper(f):
		def _wrapped(*args, **kwargs):
			with lck:
				return f(*args, **kwargs)
	return _serialized_wrapper

class CardFS(Fuse):
	"""
	"""

	def __init__(self, *args, **kw):
		self.client, self.card = None, None
		Fuse.__init__(self, *args, **kw)
		
		self.file_class = self.File
		
		print 'waiting for card...'
		self.client = client = chipcard.Client()
		client.startWait()
		self.card = card = client.getNextCard(10)
		if card is None:
			raise RuntimeError("No card found")
		print 'reading card ATR...'
		atr = self.card.atr
		TS = ord(atr[0])
		if TS not in (0x3B, 0x3F):
			# Non-standard synchronization byte -> should be a pure memory card -> no historical bytes, assume simple defaults
			self.support = { 'abspath': False, 'relpath': False, 'fcp': False, 'fmd':False, 'ef-select': False }
			
		else:
			T0 = ord(atr[1])
			hist_count = T0 & 0x0F
			i = 2
			Ti = T0
			while Ti & 0xF0 != 0:
				if Ti & 16:
					i += 1 # TAi
				if Ti & 32:
					i += 1 # TBi
				if Ti & 64:
					i += 1 # TCi
				if Ti & 128:
					Ti = ord(atr[i])
					i += 1 # TDi
				else:
					Ti = 0
			hist = atr[i:i+hist_count]
			self.historical_bytes = hist
			TCK = ord(atr[i+hist_count]) if len(atr) > i+hist_count else None
			if TCK != reduce((lambda x,y: x^y), (ord(b) for b in atr[1:i+hist_count])):
				print >>sys.stderr, "Warning: Card has invalid ATR (checksum failed)"
				
			# TODO: parse historical bytes
			
			self.support = { 'abspath': True, 'relpath': True, 'fcp': True, 'fmd':True, 'ef-select': True }
		
		self._stat = {}
		self.aliases = {}
		self.cwd = []
		self.ef = None
		fcpsuccessful = False
		print 'determining file selecting capabilities...'
		try:
			if self.support['fcp']:
				# SELECT ROOT and get FCP
				r = card.execute(0xA4, 0, 4, '\x3F\x00', 0xFF)
				fcpsuccessful = True
				self._update_stat((), r)
		except chipcard.CardException, x:
			# this should only fail if FCP retrival is unsupported
			self.support['fcp'] = False
		
		try:
			if self.support['fmd']:
				# SELECT ROOT and get FMD
				r = card.execute(0xA4, 0, 8, '\x3F\x00', 0xFF)
		except chipcard.CardException, x:
			# this should only fail if FMD retrival is unsupported
			self.support['fmd'] = False
		
		print 'select card root'
		try:
			# SELECT ROOT and get FCI
			r = card.execute(0xA4, 0, 0, '\x3F\x00', 0)
			if not fcpsuccessful:
				self._update_stat((), r)
		except chipcard.CardException, x:
			# SELECT ROOT should never fail!
			raise RuntimeError("Root directory couldn't be selected, aborting!")
		
		print 'reading EF.DIR...'
		try:
			self.read_efdir()
		except chipcard.CardException:
			print 'EF.DIR not found.'
			pass
		
		r = card.execute(0xA4, 0, 0, '\x3F\x00', 0)
		print 'ready.'
	
	def __del__(self):
		if self.card:
			self.card.free()
		if self.client:
			self.client.free()

	def fsinit ( self ):
		pass

	def read_efdir(self):
		'''
			Selects and reads the EF.DIR file and updates the global aliases list
		'''
		card = self.card
		# Move to root
		self.move_to([])
		# Select EF.DIR
		r = self._select_ef('\x2F\x00')
		self._update_stat(('DIR',), r)
		
		# Read EF.DIR file
		r = card.execute(0xB0, 0, 0, None, 0xFF)
		# Prepare for results, clear aliases list
		self.aliases = aliases = {}
		result = []
		
		# Parse directory entries
		files = parseDIR(r)
		for f in files:
			if f['AID']:
				result.append(fuse.Direntry('#'+hexcode(f['AID'])))
				aliases['#'+hexcode(f['AID'])] = f
			if f['label']:
				result.append(fuse.Direntry(f['label']))
				aliases[f['label']] = f
			for r in f['ref']:
				if len(r) == '1':
					result.append(fuse.Direntry(hexcode(r)))
					aliases[hexcode(r)] = f
				if len(r) == '2':
					result.append(fuse.Direntry(':'+hexcode(r)))
					aliases[':'+hexcode(r)] = f
		return result

	def resolve_alias(self, name):
		'''
			Resolves an alias name to an lower-level identifier if possible
		'''
		if name not in self.aliases:
			return None
		item = self.aliases[name]
		
		for ref in item['ref']:
			if len(ref) == 4:
				new0 = ':'+hexcode(item['ref'][0])
				if new0 != name:
					return new0
				else:
					break
				
		else:
			for ref in item['ref']:
				if len(ref) == 2:
					new0 = hexcode(item['ref'][0])
					if new0 != name:
						return new0
					else:
						break
			else:
				if not name.startswith(('#', ':')):
					return '#'+hexcode(item['AID'])
		
		return None

	def parse_path_part(self, part, allow_dfname=False, allow_label=False):
		# Long identifier
		if part.startswith(':') and len(part) == 5 and all(c in '1234567890ABCDEF' for c in part[1:]):
			fid = int(part[1:], 16)
			if fid == 0x3f00:
				raise ValueError(':3F00 root identifier not allowed in path')
			if fid == 0x3fff:
				raise ValueError(':3FFF identifier not allowed in path')
			return chr(fid>>8)+chr(fid%256)
		# Short identifier
		elif len(part) == 2 and all(c in '1234567890ABCDEF' for c in part):
			return chr(int(part, 16))
		# DF name
		elif allow_dfname and part.startswith('#') and len(part) % 2 == 1 and all(c in '1234567890ABCDEF' for c in part[1:]):
			name = part[1:]
			return ''.join(chr(int(name[i:i+2], 16)) for i in xrange(0, len(name), 2))
		elif allow_dfname and part == 'ATR':
			return '\x2F\x01'
		elif allow_dfname and part == 'DIR':
			return '\x2F\x00'
		# Label
		elif allow_label:
			name = self.resolve_alias(part)
			if name:
				return self.parse_path_part(name, True)
		
		if part == '.raw':
			return ''
		raise ValueError('invalid path')

	def parse_path(self, path):
		path = split_path(path)
		# Empty path -> return empty path
		if not path:
			return path
		
		# First part can be a file identifier, a DF name or a label
		fst = self.parse_path_part(path[0], True, True)
		# Parse rest
		path = [fst] + [self.parse_path_part(part) for part in path[1:]]
		
		return path

	@serialized(mutex)
	def readlink(self, path):
		print 'reading', path
		parts = split_path(os.path.normpath(path))
		if len(parts) == 1:
			result = self.resolve_alias(parts[0])
			if result:
				return './'+result
		return -errno.EINVAL
	
	@serialized(mutex)
	def getattr(self, path):
		"""
		- st_mode (protection bits)
		- st_ino (inode number)
		- st_dev (device)
		- st_nlink (number of hard links)
		- st_uid (user ID of owner)
		- st_gid (group ID of owner)
		- st_size (size of file, in bytes)
		- st_atime (time of most recent access)
		- st_mtime (time of most recent content modification)
		- st_ctime (platform dependent; time of most recent metadata change on Unix,
					or the time of creation on Windows).
		"""
		card = self.card
		
		# Parse path
		try:
			path = self.parse_path(path)
		except ValueError:
			return -errno.ENOENT
		
		# Handle root
		if not path:
			return _Stat(0, 0, 0, 040777, 2, 0, 0, 0, 0, 0, 0, 0, 0)
		
		target_ef = None
		
		# ATR special file
		if len(path) == 1 and path[0] == '\x2f\x01':
			if ('ATR',) not in self._stat:
				self.move_to([])
				# SELECT EF.ATR
				target_ef = '\x2F\x01'
				r = self._select_ef('\x2F\x01')
				self._update_stat(('ATR',), r)
			stat = self._stat.get(('ATR',), None)
		
		# DIR special file
		elif len(path) == 1 and path[0] == '\x2f\x00':
			if ('DIR',) not in self._stat:
				self.move_to([])
				# SELECT EF.DIR
				target_ef = '\x2F\x00'
				r = self._select_ef('\x2F\x00')
				self._update_stat(('DIR',), r)
			stat = self._stat.get(('DIR',), None)
		elif len(path) == 1 and self.resolve_alias(path[0]) is not None:
			# Alias -> symlink
			return _Stat(0, 0, 0, S_IFLNK, 1, 0, 0, 0, 0, 0, 0, 0, 0)
		
		# All other files
		else:
			
			if len(path) > 1 and path[-1] == '':
				path = path[:-1]
				target_ef = path[-1]
			if len(path) == 1 and path[-1] == '':
				path = []
				target_ef = ''
			
			
			# consult cache
			if tuple(path) in self._stat:
				stat = self._stat[tuple(path)]
				if stat['category'] != 'DF':
					target_ef = path[-1]
					path = path[:-1]
			# if not in cache select file to get stat data
			else:
				if self.cwd != path:
					try:
						r = self.move_to(path)
						self.cwd = path
						if r.status=='\x90\x00':
							target_ef = ''
					except chipcard.CardException, x:
						if target_ef is None:
							try:
								self.move_to(path[:-1])
								self.cwd = path[:-1]
								self._select_ef(path[-1])
								target_ef = ''
							except chipcard.CardException, x:
								raise
						else:
							raise
				if target_ef:
					r = self._select_ef(target_ef)
					self._update_stat(tuple(path)+(target_ef,), r)
					stat = self._stat.get(tuple(path)+(target_ef,), None)
				else:
					stat = self._stat.get(tuple(path), None)  
		
		links = 1
		         
		if stat:
			size = 0
			if stat['size']:
				size = stat['size']
			elif stat['datasize']:
				size = stat['datasize']
			elif stat['maxrecordsize'] and stat['recordcount']:
				size = stat['maxrecordsize'] * stat['recordcount']
			else:
				size = 4096
			
			acl = 0444 | S_IFREG
			if stat['active']:
				acl |= 0100
			if stat['category'] == 'DF':
				acl |= 0111 | S_IFDIR
				links = 2
		else:
			print 'target', repr(target_ef)
			size = 4096
			acl = 0444 | S_IFREG
			if target_ef is not None:
				try:
					# Read File Raw
					r = card.execute(0xB0, 0, 0, None, 0)
					size = len(r)
				except chipcard.CardException:
					raise

		return _Stat(0, 0, 0, acl, links, 0, 0, 0, size, 0, 0, 0, 0)
		
	
	def _update_stat(self, path, data, is_df=None):
		if len(data) < 5:
			return
		if data.status[0] == '\x90':
			if data.data.startswith(('\x62', '\x6f')):
				self._stat[tuple(path)] = x = parseFileInfo(data)
				if is_df:
					x['category'] = 'DF'
				return x
		elif data.data.endswith('\x6A\x82'):
			if [tuple(path)] in self._stat:
				del self._stat[tuple(path)]
		return None
	
	def move_to(self, to):
		result = self._move_from_to(self.cwd, to)
		self.cwd = to
		return result
	
	def _move_from_to(self, frm, to):
		print 'move from', frm, 'to', to
		card = self.card
		
		current = frm[:]
		common = []
		for i in xrange(min(len(to), len(frm))):
			if frm[i] == to[i]:
				common.append(frm[i])
			else:
				break
		
		p2 = 4 if self.support['fcp'] else 0
		if frm == to:
			return chipcard.CardResponse('\x90\x00')
		
		if len(to) == 0:
			# root
			r = card.execute(0xA4, 0, p2, '\x3F\x00', 0)
			self._update_stat((), r)
			return r
		
		# to descendant
		if len(common) == len(frm):
			# to subdir
			if len(to) == len(frm)+1:
				# try simple child selections
				if len(to[-1]) > 2:
					r = card.execute(0xA4, 4, p2, to[-1], 0)
				else:
					r = card.execute(0xA4, 1, p2, to[-1], 0)
				self._update_stat(to, r, is_df=True)
				return r
			
			# try relative path selection when 2-byte-identifiers
			if self.support['relpath'] and all(len(x) == 2 for x in to[len(common):]):
				try:
					path = ''.join(to[len(common):])
					r = card.execute(0xA4, 9, p2, path, 0)
					self._update_stat(to, r)
					return r
				except chipcard.CardException, x:
					err = x.errno
					if err == -errno.EPROTOTYPE:
						self.support['relpath'] = False
					if err == -errno.ENOENT:
						raise
			
			# try moving to the directory with simple child selections:
			tomove = to[len(current):]
			try:
				while len(tomove) > 0:
					if len(tomove[0]) > 2:
						r = card.execute(0xA4, 4, p2, tomove[0], 0)
					else:
						r = card.execute(0xA4, 1, p2, tomove[0], 0)
					self._update_stat(current+tomove[0], r, is_df=True)
					current.append[tomove.pop(0)]
				return r
			except chipcard.CardException, x:
				err = x.errno
				# Revert
				self._move_from_to(current, frm)
				current = frm
				if err == -errno.ENOENT:
					raise
		
		# to ascendant
		if len(common) == len(to):
			# use moving to the directory with parent selection:
			while len(current) > len(to):
				try:
					r = card.execute(0xA4, 3, p2, None, 0)
					self._update_stat(current[:-1], r, is_df=True)
					return r
				except chipcard.CardException, x:
					err = x.errno
					# Revert
					self._move_from_to(current, frm)
					raise
				current.pop()
		
		# Try absolute path selection when all 2-byte-identifiers
		if self.support['abspath'] and all(len(x) == 2 for x in to):
			try:
				path = ''.join(to[len(common):])
				r = card.execute(0xA4, 8, p2, path, 0)
				self._update_stat(to, r)
				return r
			except chipcard.CardException, x:
				err = x.errno
				if err == -errno.EPROTOTYPE:
					self.support['abspath'] = False
		
		if len(common) != len(frm) and len(common) != len(to):
			# Move to common anchestor and then to the descendant target
			if self._move_from_to(current, common):
				return self._move_from_to(common, to)
		
		if len(frm) > 0 and len(to) > 0:
			# Last resort: move to root and then to target
			r = self._move_from_to(frm, [])
			self._update_stat(to, r)
			return self._move_from_to([], to)
		
		return RuntimeError("move not successful")
		
	def _select_ef(self, id, record='first'):
		card = self.card
		
		p2 = 4 if self.support['fcp'] else 0
		p2 |= { 'first': 0, 'last': 1, 'next': 2, 'previous': 3, 'prev':3, None:0}[record]
		
		if len(id) > 2:
			r = card.execute(0xA4, 4, p2, id, 0)
		else:
			try:
				r = card.execute(0xA4, 2, p2, id, 00)
				self._update_stat(self.cwd+[id], r)
			except chipcard.CardException, x:
				err = x.errno
				if not self.support['ef-select']:
					# select file (generic)
					r = card.execute(0xA4, 0, p2, id, 00)
					self._update_stat(self.cwd+[id], r)
				else:
					raise
		self.ef = id
		return r
		
	@serialized(mutex)
	def readdir(self, path, offset):
		result = [fuse.Direntry('.'), fuse.Direntry('..')]
		path = split_path(os.path.normpath(path))
		
		if len(path) == 0:
			try:
				card = self.card
				self.move_to([])
				
				# SELECT EF.ATR
				try:
					r = self._select_ef('\x2F\x01')
					self._update_stat(('ATR',), r)
					result.append( fuse.Direntry('ATR') )
				except chipcard.CardException, x:
					pass
				
				# SELECT EF.DIR
				try:
					r = self._select_ef('\x2F\x00')
					self._update_stat(('DIR',), r)
					result.append( fuse.Direntry('DIR') )
				except chipcard.CardException, x:
					if x.errno == errno.ENOENT or x.errno == errno.EINVAL:
						return -errno.EACCES
					else:
						raise
				
				result.append( fuse.Direntry('.raw') )
				
				# READ DIR
				r = card.execute(0xB0, 0, 0, None, 0)
				
				self.aliases = aliases = {}
				
				files = parseDIR(r)
				for f in files:
					if f['AID']:
						result.append(fuse.Direntry('#'+hexcode(f['AID'])))
						aliases['#'+hexcode(f['AID'])] = f
					if f['label']:
						result.append(fuse.Direntry(f['label']))
						aliases[f['label']] = f
					for r in f['ref']:
						if len(r) == '1':
							result.append(fuse.Direntry(hexcode(r)))
							aliases[hexcode(r)] = f
						if len(r) == '2':
							result.append(fuse.Direntry(':'+hexcode(r)))
							aliases[':'+hexcode(r)] = f
			except Exception, e:
				raise
				return -errno.ENOENT
		return result

	def statfs ( self ):
		print '*** statfs'
		return -errno.ENOSYS

	@property
	def File(self):
		class File:
			
			fs = self
			
			@serialized(mutex)
			def __init__(self, path, flags, *mode):
				fs = self.fs
				# Reject direct io because it is unsupported
				if (flags & os.O_DIRECT): raise IOError(errno.EINVAL, 'unsupported')
				
				path = fs.parse_path(path)
				stat = fs._stat.get(tuple(path), None)
				fs.move_to(path[:-1])
				if path[-1] == '':
					try:
						fs._select_ef(path[-2])
					except Exception:
						pass
				else:
					fs._select_ef(path[-1])
				
				if not stat or not stat['structure'].startswith(('linear', 'cyclic')):
					self.contents = fs.card.execute(0xB0, 0, 0, None, 0)
				else:
					self.contents = []
					max = stat.get('recordcount', 254)
					size = stat.get('maxrecordsize', 0)
					for i in xrange(1, max+1):
						x = fs.card.execute(0xB2, i, 4, None, 0)
						if len(x) > size:
							size = len(x)
						self.contents.append(x)
					self.recordsize = size
			
			keep_cache = True
			direct_io = False
			
			@serialized(mutex)
			def read(self, length=0, offset=0):
				if isinstance(self.contents, str):
					return str(self.contents[offset:offset+length])
				else:
					record = offset // self.recordsize
					offset = offset % self.recordsize
					return str(self.contents[record][offset:offset+length])
			
			@serialized(mutex)
			def write(self, buf, offset):
				raise IOError(errno.EACCES, "TODO")
			
			@serialized(mutex)
			def fsync(self, isfsyncfile):
				pass
			
			@serialized(mutex)
			def flush(self):
				pass
			
		return File
	


if __name__ == '__main__':
	fs = CardFS()
	fs.flags = 0
	fs.multithreaded = 0
	fs.parse(sys.argv)
	fs.main()

